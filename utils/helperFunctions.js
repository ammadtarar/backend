'use strict';

global.checkAdminLevelOneAccess = (req, message) => {
    if (req.admin.access_level != 1) {
        throw new CustomError(message, 403)
    }
};