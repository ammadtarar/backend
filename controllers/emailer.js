const nodemailer = require("nodemailer");
let sgTransport = require('nodemailer-sendgrid-transport');
var handlebars = require("handlebars");
var fs = require("fs");


let options = {
    auth: {
        api_key: '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'
    }
}
let transporter = nodemailer.createTransport(sgTransport(options));

String.prototype.replaceAll = function (str1, str2, ignore) {
    return this.replace(new RegExp(str1.replace(
        /([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, "\\$&"), (ignore ?
            "gi" : "g")), (typeof (str2) == "string") ? str2.replace(/\$/g, "$$$$") :
        str2);
}

var readHTMLFile = function (path, callback) {
    fs.readFile(path, { encoding: "utf-8" }, function (err, html) {
        if (err) {
            throw err;
            callback(err);
        } else {
            callback(null, html);
        }
    });
};



async function sendAdminCreationEmail(name, email, password) {
    new Promise((resolve, reject) => {
        readHTMLFile(__dirname + "/../htmls/admin.html", function (err, html) {
            transporter
                .sendMail({
                    from: '"NxGen Labs" <info@nxgenlabs.com.pk>',
                    to: email,
                    subject: "You have been given admin access to NxGen Admin Panel",
                    attachments: [],
                    html: handlebars.compile(html)({
                        name: name,
                        email: email,
                        password: password,
                        url: process.env.ADMIN_URL,
                    }),
                })
                .then((success) => {
                    console.log("email send success");
                    console.log(success);
                    resolve(success);
                })
                .catch((err) => {
                    console.log("email send fail");
                    console.log(err);
                    reject(err);
                });
        });
    });
};

async function sendPatientOTP(email, otp) {
    new Promise((resolve, reject) => {
        readHTMLFile(__dirname + "/../htmls/patient_otp.html", function (err, html) {
            transporter
                .sendMail({
                    from: '"NxGen Labs" <info@nxgenlabs.com.pk>',
                    to: email,
                    subject: "NxGen Labs OTP",
                    attachments: [],
                    html: handlebars.compile(html)({
                        otp: otp
                    }),
                })
                .then((success) => {
                    console.log("email send success");
                    console.log(success);
                    resolve(success);
                })
                .catch((err) => {
                    console.log("email send fail");
                    console.log(err);
                    reject(err);
                });
        });
    });
};

async function sendEmailToPatient(email, subject, name, message) {
    new Promise((resolve, reject) => {
        readHTMLFile(__dirname + "/../htmls/patient_rider_assigned.html", function (err, html) {
            transporter
                .sendMail({
                    from: '"NxGen Labs" <info@nxgenlabs.com.pk>',
                    to: email,
                    subject: subject,
                    attachments: [],
                    html: handlebars.compile(html)({
                        name: name,
                        message: message
                    }),
                })
                .then((success) => {
                    console.log("email send success");
                    console.log(success);
                    console.log('==========');
                    resolve(success);
                })
                .catch((err) => {
                    console.log("email send fail");
                    console.log(err);
                    console.log('==========');
                    reject(err);
                });
        });
    });
};


async function sendEmailToAdmin(subject, message) {
    console.log('SENDING EMAIL');
    new Promise((resolve, reject) => {
        readHTMLFile(__dirname + "/../htmls/booking_notification_to_admin.html", function (err, html) {
            transporter
                .sendMail({
                    from: '"NxGen Labs" <info@nxgenlabs.com.pk>',
                    to: ['info@nxgenlabs.com.pk', 'inquiries@nxgenlabs.com.pk'],
                    subject: subject,
                    attachments: [],
                    html: handlebars.compile(html)({
                        message: message
                    }),
                })
                .then((success) => {
                    console.log("email send success");
                    console.log(success);
                    resolve(success);
                })
                .catch((err) => {
                    console.log("email send fail");
                    console.log(err);
                    reject(err);
                });
        });
    });
};


module.exports = {
    sendAdminCreationEmail,
    sendPatientOTP,
    sendEmailToPatient,
    sendEmailToAdmin
}