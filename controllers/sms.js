const axios = require('axios');
const https = require('https')

sendSms = (message, phone) => {

    console.log('========');
    console.log('SENDING SMS TO = ', phone);
    return new Promise((resolve, reject) => {

        var parsedPhone = phone;
        if (parsedPhone.substring(0, 2) == '03') {
            parsedPhone = parsedPhone.substring(1);
        }

        const instance = axios.create({
            httpsAgent: new https.Agent({
                rejectUnauthorized: false
            })
        });

        instance.post('https://cbs.zong.com.pk/reachrestapi/home/SendQuickSMS', {
            "loginId": "923114463540",
            "loginPassword": "Zong@123",
            "Destination": `92${parsedPhone}`,
            "Mask": "NXGEN LAB",
            "Message": message,
            "UniCode": "0",
            "ShortCodePrefered": "n"
        })
            .then(res => {
                console.log('SUCCESS');
                console.log(res.data);
                console.log('=======');
                resolve({
                    data: true,
                    error: null
                })
            })
            .catch(err => {
                console.log('ERROR');
                console.error(err);
                console.log('=======');
                resolve({
                    data: null,
                    error: err
                })
            })
    });
}

module.exports = {
    sendSms
}