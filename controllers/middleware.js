var cryptojs = require('crypto-js');

const colors = {
    Reset : "\x1b[0m",
    Bright : "\x1b[1m",
    Dim : "\x1b[2m",
    Underscore : "\x1b[4m",
    Blink : "\x1b[5m",
    Reverse : "\x1b[7m",
    Hidden : "\x1b[8m",

    FgBlack : "\x1b[30m",
    FgRed : "\x1b[31m",
    FgGreen : "\x1b[32m",
    FgYellow : "\x1b[33m",
    FgBlue : "\x1b[34m",
    FgMagenta : "\x1b[35m",
    FgCyan : "\x1b[36m",
    FgWhite : "\x1b[37m",

    BgBlack : "\x1b[40m",
    BgRed : "\x1b[41m",
    BgGreen : "\x1b[42m",
    BgYellow : "\x1b[43m",
    BgBlue : "\x1b[44m",
    BgMagenta : "\x1b[45m",
    BgCyan : "\x1b[46m",
    BgWhite : "\x1b[47m",

    HEADER    : '\033[95m',
    OKBLUE    : '\033[94m',
    OKGREEN   : '\033[92m',
    WARNING   : '\033[93m',
    FAIL      : '\033[91m',
    ENDC      : '\033[0m', 
    BOLD      : '\033[1m',   
    UNDERLINE : '\033[4m%s'
};

module.exports = function (db) {
    return {
        errorHandler: function (e, req, res, next) {
            if (e instanceof db.Sequelize.ForeignKeyConstraintError) {
                res.status(409).json({
                    message: "Incorrect ID passed. Please make sure the ID is valid and exists"
                })
            } else if (e instanceof db.Sequelize.ValidationError) {
                const messages = {};
                e.errors.forEach((error) => {
                    let message;
                    switch (error.validatorKey) {

                        case 'isEmail':
                            message = error.value + ' is not a valid email address. Please enter a valid email';
                            break;
                        case 'isDate':
                            message = 'Please enter a valid date';
                            break;
                        case 'len':
                            if (error.validatorArgs[0] === error.validatorArgs[1]) {
                                message = 'Use ' + error.validatorArgs[0] + ' characters for ' + error.path;
                            } else {
                                message = 'Use between ' + error.validatorArgs[0] + ' and ' + error.validatorArgs[1] + ' characters for ' + error.path;
                            }
                            break;
                        case 'min':
                            message = 'Use a number greater or equal to ' + error.validatorArgs[0] + 'for ' + error.path;
                            break;
                        case 'max':
                            message = 'Use a number less or equal to ' + error.validatorArgs[0] + 'for ' + error.path;
                            break;
                        case 'isInt':
                            message = 'Please use an integer number for ' + error.path;
                            break;
                        case 'is_null':
                            message = error.path + ' is required. Please complete ' + error.path + ' field';
                            break;
                        case 'not_unique':
                            message = error.value != null ? error.value + ' is taken. Please choose another ' + error.path : error.message;
                            break;
                        case 'isUrl':
                            message = error.value != null ? error.value + ' is not a valid value for ' + error.path : error.message;
                            break;
                    }
                    console.log("=== e ===");
                    console.log(e);
                    messages["message"] = message;
                });
                console.log("==e==");
                console.log(e);
                res.status(422).json(messages)
            } else if (e instanceof EnumValidationError) {

                res.status(422).json({
                    message: `${e.enteredValue} is not a valid value for ${e.field}. The allowed values are : ${e.allowedValues}`
                })
            } else if (e instanceof CustomError) {
                console.log('Custom error');
                console.log(e);
                res.status(e.code).json({
                    message: e.message
                })

            } else {
                console.log("");
                console.log("");
                console.log("=====");
                console.log("instance of = ", e.name);
                console.log(e);
                console.log(e.status);
                console.log(e.message);


                res.status(e.status || 500).json({
                    message: e.message
                })
            }
        },
        logger: function (req, res, next) {
            next();       



            console.log("");
            console.log("");
            console.log("");
            console.log(`${colors.FgYellow}${colors.BOLD}========================================================================${colors.ENDC}`);
            console.log(`${colors.FgYellow}${colors.BOLD}==================== ${colors.OKGREEN}${colors.BOLD}I N C O M I N G   R E Q U E S T ${colors.FgYellow}===================${colors.ENDC}`);
            console.log(`${colors.FgYellow}${colors.BOLD}========================================================================${colors.ENDC}`);
            console.log("");
            console.log(`${colors.FgYellow}${colors.BOLD}* DATE :`,colors.ENDC);
            console.log("");
            console.log(`${new Date().toString()}`);
            console.log("");
            console.log("");
            console.log(`${colors.FgYellow}${colors.BOLD}* URL :`,colors.ENDC);
            console.log("");
            console.log(`${colors.FgCyan}${colors.BOLD}${req.method}`,  "\x1b[37m" , `- ${colors.ENDC}${colors.Dim}${req.protocol}://${req.headers.host}${colors.ENDC}${colors.BOLD}${req.baseUrl}${req.path}`);
            console.log("");
            console.log("");
            console.log(`${colors.FgYellow}${colors.BOLD}* PARAMS :`,colors.ENDC);
            console.log("");
            console.log(JSON.parse(JSON.stringify(req.params)));
            console.log("");
            console.log("");
            console.log(`${colors.FgYellow}${colors.BOLD}* QUERY :`,colors.ENDC);
            console.log("");
            console.log(JSON.parse(JSON.stringify(req.query)));
            console.log("");
            console.log("");
            console.log(`${colors.FgYellow}${colors.BOLD}* HEADERS :`,colors.ENDC);
            console.log("");
            console.log(JSON.parse(JSON.stringify(req.headers)));
            console.log("");
            console.log("");
            console.log(`${colors.FgYellow}${colors.BOLD}========================================================================${colors.ENDC}`);
            console.log("");
            console.log("");
            console.log("");
                 
        },

        authenticate: async function (req, res, next) {
            var token = req.get("Authorization") || "";
            if (token === undefined || token === "") {
                res.status(401).send({
                    message: 'Please send token in request header'
                });
                return;
            }
            let tokenModel = await db.patient_token.get({ where: { tokenHash: cryptojs.MD5(token).toString() } });
            if (tokenModel.data && tokenModel.data.patientId && !tokenModel.error) {
                let patientModel = await db.patient.get({ where: { id: tokenModel.data.patientId } });
                if (patientModel.data && !patientModel.error) {
                    req.patient = patientModel.data;
                    next();
                    return;
                }
            }



            db.admin.findOne({
                where: {
                    tokenHash: cryptojs.MD5(token).toString()
                }
            })
                .then(function (admin) {

                    if (!admin) {


                        db.rider.findOne({
                            where: {
                                tokenHash: cryptojs.MD5(token).toString()
                            }
                        })
                            .then(function (rider) {

                                if (!rider) {
                                    res.status(401).send({
                                        message: 'User not found. Please make sure the the email and passowrd are correct'
                                    });
                                    return;
                                }
                                req.rider = rider;
                                next();
                            })
                            .catch(function (err) {
                                console.log("General authentication error");
                                console.log(err);
                                res.status(401).send();
                            });


                        return;
                    }
                    req.admin = admin;
                    next();
                })
                .catch(function (err) {
                    res.status(401).send();
                });

        },
        authenticatePatient: async function (req, res, next) {
            var token = req.get("Authorization") || "";
            if (token === undefined || token === "") {
                res.status(401).send({
                    message: 'Please send token in request header'
                });
                return;
            }
            let tokenModel = await db.patient_token.get({ where: { tokenHash: cryptojs.MD5(token).toString() } });
            if (!tokenModel.data || tokenModel.error) {
                res.status(401).send({
                    message: 'Patient not found. Please make sure you are sending the correct email and password'
                });
            }
            let patientModel = await db.patient.get({ where: { id: tokenModel.data.patientId } });
            if (!patientModel.data || patientModel.error) {
                res.status(401).send({
                    message: 'Patient not found. Please make sure you are sending the correct email and password'
                });
            }
            req.patient = patientModel.data;
            next();
        },
        authenticateAdmin: function (req, res, next) {
            var token = req.get("Authorization") || "";
            if (token === undefined || token === "") {
                res.status(401).send({
                    message: 'Please send token in request header'
                });
                return;
            }
            db.admin.findOne({
                where: {
                    tokenHash: cryptojs.MD5(token).toString()
                }
            })
                .then(function (admin) {

                    if (!admin) {
                        res.status(401).send({
                            message: 'Admin not found. Please make sure you are sending the correct email and password'
                        });
                        return;
                    }
                    req.admin = admin;
                    next();
                })
                .catch(function (err) {
                    console.log("Admin authentication error");
                    console.log(err);
                    res.status(401).send();
                });
        },
        authenticateRider: function (req, res, next) {
            var token = req.get("Authorization") || "";
            if (token === undefined || token === "") {
                res.status(401).send({
                    message: 'Please send token in request header'
                });
                return;
            }
            db.rider.findOne({
                where: {
                    tokenHash: cryptojs.MD5(token).toString()
                }
            })
                .then(function (rider) {

                    if (!rider) {
                        res.status(401).send({
                            message: 'Rider not found. Please make sure you are sending the correct email and password'
                        });
                        return;
                    }
                    req.rider = rider;
                    next();
                })
                .catch(function (err) {
                    console.log("Rider authentication error");
                    console.log(err);
                    res.status(401).send();
                });
        }

    };

};