
const Sequelize = require("sequelize");

let enableLog = process.env.ENABLE_DB_LOGGING === 'true';
console.log('process.env.ENABLE_DB_LOGGING');
console.log(enableLog);

var sequelize = sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD,
    {
        host: process.env.DB_HOST,
        dialect: process.env.DB_DIALECT,
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        port: process.env.DB_PORT,
        define: {
            paranoid: false
        },
        logging: enableLog
    }
);

var db = {};

db.sequelize = sequelize;
db.Sequelize = Sequelize;
db.Op = Sequelize.Op;

db.admin = sequelize.import("../models/admin.js");
db.patient = sequelize.import("../models/patient.js");
db.patient_token = sequelize.import("../models/patient_token.js");
db.patient.hasMany(db.patient_token);

db.subscriber = sequelize.import("../models/subscriber.js");
db.feedback = sequelize.import("../models/feedback.js");
db.banner = sequelize.import("../models/banner.js");
db.rider = sequelize.import("../models/rider.js");
db.rateslist = sequelize.import("../models/rateslist.js");
db.verification_code = sequelize.import("../models/verification_code.js");

db.appointment = sequelize.import("../models/appointment.js");
db.appointment_test = sequelize.import("../models/appointment_test.js");
db.appointment.belongsTo(db.patient);
db.appointment.hasMany(db.appointment_test, { as: 'tests' });
db.appointment_test.belongsTo(db.appointment);
db.appointment.belongsTo(db.rider);

db.package = sequelize.import("../models/package.js");
db.faq = sequelize.import("../models/faq.js");



module.exports = db;
