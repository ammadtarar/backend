const GENDERS = {
    MALE: 'male',
    FEMALE: 'female',
    OTHER: 'other'
};

const APPOINTMENT_MODES = {
    HOME_SAMPLING: 'home_sampling',
    VISIT_LAB: 'visit_lab'
}

const APPOINTMENT_STATUS = {
    PENDING: 'pending',
    RIDER_ASSIGNED: 'rider_assigned',
    SAMPLE_COLLECTED: 'sample_collected',
    TESTING_IN_PROCESS: 'testing_in_process',
    COMPLETED: 'completed',
    CANCELLED: 'cancelled'
};


let CONSTANTS = { ...GENDERS, ...APPOINTMENT_MODES, ...APPOINTMENT_STATUS };

module.exports = {
    CONSTANTS: CONSTANTS,
    GENDERS: Object.values(GENDERS),
    APPOINTMENT_MODES: Object.values(APPOINTMENT_MODES),
    APPOINTMENT_STATUS: Object.values(APPOINTMENT_STATUS)
}