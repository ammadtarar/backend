const { Router } = require('express');
const app = Router();
const underscore = require('underscore');
const db = require('../../controllers/db.js');
const middleware = require('../../controllers/middleware.js')(db);

const { sendAdminCreationEmail } = require("../../controllers/emailer.js");

app.post('/register', middleware.authenticateAdmin, async (req, res, next) => {
    checkAdminLevelOneAccess(req, 'Only admin with access level 1 can create a new admin');
    let body = underscore.pick(req.body, "email", "name", "gender", "access_level");
    if (!body || Object.keys(body).length < 4) {
        throw new CustomError('Request body is missing.', 400)
    }

    let password = (Math.random() + 1).toString(36).substring(2);
    body.password = password;

    let { data, error } = await db.admin.add(body);
    if (error) {
        next(error);
        return
    }

    res.json({
        message: 'Admin created successfully',
        admin: data.toPublicJSON()
    });
    sendAdminCreationEmail(body.name, body.email, password)
});

app.post('/login', (req, res, next) => {
    const body = underscore.pick(req.body, "email", "password");
    if (!body || Object.keys(body).length < 2) {
        throw new CustomError('Request body is missing. Please send email and password in request body', 400)
    }
    let token, adminInstance;
    db.admin
        .authenticate(body)
        .then(admin => {
            adminInstance = admin;
            token = admin.generateToken('authentication');
            return db.admin.update(
                {
                    token: token,
                },
                {
                    where: {
                        id: admin.id,
                    },
                }
            );
        })
        .then(u => {
            res.header('Authentication', token);
            var returableAdmin = adminInstance.toPublicJSON();
            returableAdmin.token = token;
            res.json({
                message: 'Login successful',
                admin: returableAdmin
            })
        })
        .catch(err => {
            next(err);
        });
});


app.get('/list', middleware.authenticateAdmin, async (req, res, next) => {
    var limit = parseInt(req.query.limit) || 10;
    var page = parseInt(req.query.page) || 0;
    if (page >= 1) {
        page = page - 1;
    }
    let { data, error } = await db.admin
        .getAllAndCount({
            where: (req.body ? req.body.where : {}) || {},
            limit: limit,
            offset: limit * page,
            order: [["createdAt", "DESC"]],
            attributes: {
                exclude: ["salt", "password_hash", "tokenHash", "deletedAt"],
            },
            distinct: true,
        });

    if (error) {
        next(error);
        return;
    }
    res.json(data);
});


app.delete('/:id', middleware.authenticateAdmin, async (req, res, next) => {
    checkAdminLevelOneAccess(req, 'Only admin with access level 1 can delete an existing admin');
    var id = parseInt(req.params.id);
    if (id === undefined || id === null || id <= 0) {
        throw new CustomError('Please send admin id in url. For example /admin/:id.', 400);
    }

    let { data, error } = await db.admin.get({ where: { id: id } });
    if (error) {
        next(error)
        return
    }
    if (data == null) {
        next(new CustomError(`Admin not found. Admin with ID = ${id} does not exist`, 404));
        return
    }
    await data.destroy();
    res.json({
        message: 'Admin deleted successfully'
    });

});


module.exports = app;