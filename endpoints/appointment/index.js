const { Router } = require('express');
const app = Router();
const underscore = require('underscore');
const db = require('../../controllers/db.js');
const middleware = require('../../controllers/middleware.js')(db);
const { CONSTANTS } = require('../../controllers/constants');
const { sendSms } = require('../../controllers/sms')
const { sendEmailToPatient, sendEmailToAdmin } = require('../../controllers/emailer');
const { lastIndexOf } = require('underscore');

app.post('/create', middleware.authenticatePatient, async (req, res, next) => {
    let body = underscore.pick(req.body, 'name', 'email', 'phone', 'gender', 'lab_test_name', 'mode', 'address', 'city', 'time', 'dob');
    if (!body || Object.keys(body).length < 6) {
        throw new CustomError('Please send name , email , phone , gender , dob, lab_test_name , mode , address , city and time in the request body', 400);
    }
    body.patientId = req.patient.id;
    let tests = body.lab_test_name;
    delete body['lab_test_name']

    if (body.email && body.email == '') {
        body.email = null;
    }


    try {
        let create = await db.appointment.add(body);
        if (create.error) {
            next(create.error);
            return;
        }

        var testsBody = [];

        tests.forEach(item => {
            testsBody.push({
                appointmentId: create.data.id,
                appointment_id: create.data.id,
                name: item.name,
                test_code: item.id,
                status: CONSTANTS.PENDING
            })
        });

        let createTests = await db.appointment_test.bulkAdd(testsBody);
        if (createTests.error) {
            next(createTests.error);
            return;
        }

        if (process.env.NODE_ENV != 'local') {
            let patient = await db.patient.get({ where: { id: body.patientId } });

            var msg = '';
            if (create.data.mode == 'home_sampling') {
                msg = `Thank you for choosing nxgen labs . Your appointment has been registered . Your appointment ID is ${create.data.id}. You will shortly receive rider details`
            } else {
                msg = `Thank you for choosing nxgen labs . Your appointment has been registered . Your appointment ID is ${create.data.id}.`
            }
            if (create.data) {
                if (create.data.phone) {
                    await sendSms(msg, create.data.phone);
                }
                if (create.data.email) {
                    await sendEmailToPatient(
                        create.data.email,
                        'NxGen Labs - Appointment registered',
                        create.data.name,
                        msg
                    );
                }
            }
            await sendEmailToAdmin('New Appointment Notification', `Someone has created a new appointment from NxGen app/web. The appointment id = ${create.data.id}. Please open the admin website to process the appointment`)
        }
        res.json({
            message: 'Appointment created successfully'
        });
    } catch (error) {
        next(error)
    }


});


app.get('/list', middleware.authenticate, async (req, res, next) => {
    var limit = parseInt(req.query.limit) || 10;
    var page = parseInt(req.query.page) || 0;
    if (page >= 1) {
        page = page - 1;
    }
    var where = {};
    if (req.patient) {
        where.patient_id = req.patient.id;
    }
    var rawWhere = {};
    if (req.query && req.query.where) {
        rawWhere = JSON.parse(req.query.where)
    }
    if (Object.keys(rawWhere).length > 0) {
        Object.keys(rawWhere).forEach(e => {
            if (e === 'name') {
                where[e] = {
                    [db.Op.like]: `%${rawWhere[e]}%`
                }
            } else {
                where[e] = rawWhere[e];
            }
        })
    }


    if (req.rider) {
        where.riderId = req.rider.id;
    }


    let query = {
        where: where || {},
        limit: limit,
        offset: limit * page,
        order: [["createdAt", "DESC"]],
        distinct: true,
        attributes: {
            exclude: ['patient_Id']
        },
        include: [
            {
                model: db.patient,
                as: 'patient',
                attributes: {
                    exclude: ["password_hash", "salt", "tokenHash", "createdAt", "updatedAt"]
                }
            },
            {
                model: db.appointment_test,
                as: 'tests',
                attributes: {
                    exclude: ['appointmentId', 'createdAt', 'updatedAt']
                }
            },
            {
                model: db.rider,
                as: 'rider',
                attributes: {
                    exclude: ['username', 'createdAt', 'updatedAt']
                }
            }
        ]
    };

    let { data, error } = await db.appointment.getAllAndCount(query);
    if (error) {
        next(error);
        return
    }
    res.json(data);



});


app.get('/:id', middleware.authenticatePatient, async (req, res, next) => {
    let id = parseInt(req.params.id);
    if (id === undefined || id === null || id <= 0) {
        throw new CustomError('Please send appointment id in url. For example /appointment/:id.', 400);
    }

    let appointment = await db.appointment.get({
        where: { id: id },
        include: [
            {
                model: db.appointment_test,
                as: 'tests',
                attributes: {
                    exclude: ['appointmentId', 'createdAt', 'updatedAt']
                }
            }
        ]
    });
    if (!appointment.data || appointment.error) {
        next(new CustomError(`Appointment not found`, 404))
        return
    }
    res.json(appointment.data);

});


app.delete('/:id', middleware.authenticate, async (req, res, next) => {
    let id = parseInt(req.params.id);
    if (id === undefined || id === null || id <= 0) {
        throw new CustomError('Please send appointment id in url. For example /appointment/:id.', 400);
    }

    if (req.admin) {
        checkAdminLevelOneAccess(req, 'Only admin with access level 1 can delete appointments');
    }

    let where = { where: { id: id } };
    let { data, error } = await db.appointment.get(where);

    if (error) {
        next(error);
        return
    }
    if (!data) {
        next(new CustomError(`Appointment not found. No appointment with ID = ${id} was found. Please make sure the appointment id is correct and the appointment exists`, 404))
        return
    }
    if (req.patient && data.patientId != req.patient.id) {
        next(new CustomError(`This appointment does not belongs to you. You cannot delete this appointment`, 404))
        return
    }
    await data.destroy();

    res.json({
        message: 'Appointmentas deleted successfully'
    });

});

app.patch('/:id/assign/rider/:riderId', middleware.authenticateAdmin, async (req, res, next) => {
    let id = parseInt(req.params.id);
    if (id === undefined || id === null || id <= 0) {
        return next(new CustomError('Please send appointment id in url. For example /appointment/:appointmentId/assign/rider/:riderId.', 400));
    }

    let riderId = parseInt(req.params.riderId);
    if (riderId === undefined || riderId === null || riderId <= 0) {
        return next(new CustomError('Please send rider id in url. For example /appointment/:appointmentId/assign/rider/:riderId.', 400));
    }

    console.log('riderId = ', riderId);
    let rider = await db.rider.get({ where: { id: riderId } });
    console.log(rider);
    if (!rider.data || rider.error) {
        return next(new CustomError('Rider not found', 404));
    }

    let appointment = await db.appointment.get({
        where: { id: id },
        include: [
            {
                model: db.patient,
                as: 'patient',
                attributes: {
                    exclude: ["password_hash", "salt", "tokenHash", "createdAt", "updatedAt"]
                }
            }
        ]
    });
    if (!appointment.data || appointment.error) {
        return next(new CustomError('Appoointment not found', 400))
    }



    appointment.data.setRider(riderId)
        .then(async () => {
            if (process.env.NODE_ENV != 'local') {
                await sendSms('[NxGen Labs] - You have a new sample pickup appointment. Please check your NxGen Rider app for details', rider.data.phone);
                let patient = appointment.data.patient;
                if (patient) {
                    if (patient.phone) {
                        // SEND 'RIDER ASSIGNED' SMS TO THE PERSON WHO REGISTERED THE ACCOUNT
                        if (appointment.data.phone && appointment.data.name) {
                            console.log("🚀 ~ file: index.js ~ line 269 ~ .then ~ appointment.data.phone", appointment.data.phone)
                            // SEND 'RIDER ASSIGNED' SMS TO THE PATIENT NUMBER DECLARED DURING THE APPOINTMENT 
                            await sendSms(`Dear ${appointment.data.name}, a rider has been assigned against booking ID # ${appointment.data.id} . Our rider (${rider.data.name}) will contact you soon.`, appointment.data.phone);
                        }
                    }
                    if (patient.email) {
                        await sendEmailToPatient(
                            patient.email,
                            'NxGen Labs - Rider assigned to your appointment',
                            patient.name,
                            `A rider has been assigned against booking ID # ${appointment.data.id} . Our rider (${rider.data.name}) will contact you soon.`
                        );
                    }
                }
            }
            res.json({
                message: 'Rider successfully assigned to the appointment'
            });

        })
        .catch(err => {
            next(err);
        });

});

app.patch('/:id', middleware.authenticate, (req, res, next) => {
    let id = parseInt(req.params.id);
    if (id === undefined || id === null || id <= 0) {
        throw new CustomError('Please send appointment id in url. For example /appointment/:id.', 400);
    }
    let body = underscore.pick(req.body, 'status');
    if (!body || Object.keys(body).length < 1) {
        throw new CustomError('Please send desired status in the request body', 400);
    }
    db.appointment
        .update({
            status: body.status
        }, {
            where: {
                id: id
            }
        })
        .then(e => {
            if (e != 1) {
                res.json({
                    message: `Failed to update appointment with ID = ${id}`
                });
            } else {
                res.json({
                    message: 'Appointment status updated successfully'
                });
            }
        })
        .catch(err => {
            next(err);
        });
});



module.exports = app;