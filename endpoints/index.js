const express = require('express');
const server = express();

//ROUTES
server.use('/admin', require('./admin/index.js'));
server.use('/feedback', require('./feedback/index.js'));
server.use('/subscriber', require('./subscriber/index.js'));
server.use('/patient', require('./patient/index.js'));
server.use('/appointment', require('./appointment/index.js'));
server.use('/ratelist', require('./ratelist/index.js'));
server.use('/banner', require('./banner/index.js'));
server.use('/file', require('./files/index.js'));
server.use('/rider', require('./rider/index.js'));
server.use('/package', require('./packages/index.js'));
server.use('/faq', require('./faq/index.js'));

module.exports = server;