const { Router } = require('express');
const app = Router();
const underscore = require('underscore');
const db = require('../../controllers/db.js');
const middleware = require('../../controllers/middleware.js')(db);
const { sendPatientOTP } = require("../../controllers/emailer.js");
const { sendSms } = require('../../controllers/sms');

app.post('/register', async (req, res, next) => {
    let body = underscore.pick(req.body, "email", "name", "password", "gender", "phone", "city", "address");
    if (!body || Object.keys(body).length < 5) {
        throw new CustomError('Request body is missing.', 400)
    }
    if (body.email == '') {
        body.email = null
    }
    let { data, error } = await db.patient.add(body);
    if (error) {
        next(error);
        return;
    }
    res.json({
        message: 'Patient registration successful',
        patient: data.toPublicJSON()
    });


});

app.post('/login', (req, res, next) => {
    const body = underscore.pick(req.body, "username", "password", "type");
    if (!body || Object.keys(body).length < 3) {
        throw new CustomError('Request body is missing. Please send username , type and password in request body', 400)
    }
    let token, patientInstance;
    db.patient
        .authenticate(body)
        .then(async patient => {
            patientInstance = patient;
            let tokenModel = await db.patient_token.create({ patientId: patient.id });
            token = tokenModel.generateToken('authentication');
            return db.patient_token.update(
                {
                    token: token,
                },
                {
                    where: {
                        id: tokenModel.id,
                    },
                }
            );
        })
        .then(u => {
            res.header('Authentication', token);
            var returablePatient = patientInstance.toPublicJSON();
            returablePatient.token = token;
            res.json({
                message: 'Login successful',
                patient: returablePatient
            })
        })
        .catch(err => {
            next(err);
        });
});


app.get('/list', middleware.authenticateAdmin, async (req, res, next) => {
    var limit = parseInt(req.query.limit) || 10;
    var page = parseInt(req.query.page) || 0;
    if (page >= 1) {
        page = page - 1;
    }


    var where = {};

    try {
        let rawWhere = JSON.parse(req.query.where || {}) || {};
        if (Object.keys(rawWhere).length > 0) {
            Object.keys(rawWhere).forEach(e => {
                if (['name', 'email', 'phone', 'city'].includes(e)) {
                    where[e] = {
                        [db.Op.like]: `%${rawWhere[e]}%`
                    }
                } else {
                    where[e] = rawWhere[e];
                }
            })
        }
    } catch (error) {
        console.log('Parsing error in wher in patients');
    }

    let { data, error } = await db.patient
        .getAllAndCount({
            where: where,
            limit: limit,
            offset: limit * page,
            order: [["createdAt", "DESC"]],
            attributes: {
                exclude: ["salt", "password_hash", "tokenHash", "deletedAt"],
            },
            distinct: true,
        });

    if (error) {
        next(error);
        return
    }
    res.json(data)
});


app.delete('/:id', middleware.authenticateAdmin, async (req, res, next) => {
    var id = parseInt(req.params.id);
    if (id === undefined || id === null || id <= 0) {
        throw new CustomError('Please send patient id in url. For example /patient/:id.', 400);
    }

    let patient = await db.patient.get({ where: { id: id } });
    if (patient.error) {
        next(patient.error);
        return;
    }

    if (patient.data == null) {
        next(new CustomError(`Patient not found. Patient with ID = ${id} does not exist`, 404));
        return
    }
    await patient.data.destroy();
    res.json({
        message: 'Patient deleted successfully'
    });
});

app.patch('/send/code', async (req, res, next) => {
    let type = req.query.type;
    console.log(req.query);
    if (!type) {
        return next(new CustomError(`Please send type in request url qeruy`, 404));
    }
    if (type != 'email' && type != 'phone') {
        return next(new CustomError(`Type can only be email or phone`, 404));
    }
    let destination = req.query.destination;
    if (!destination) {
        return next(new CustomError(`Please send destination (email address or phone) in request url qeruy`, 404));
    }
    let code = await db.verification_code.add({ destination: destination });
    if (code.error) {
        next(error);
        return
    }
    if (process.env.NODE_ENV != 'local') {
        type == 'email' ? await sendPatientOTP(destination, code.data.code) : await sendSms(`[NxGen Labs] You OTP code is ${code.data.code}`, destination);
        res.json({
            message: 'OTP sent successfully '
        })
    } else {
        res.json({
            code: code.data.code,
            message: 'OTP sent successfully '
        })
    }

});


app.post('/reset/password', async (req, res, next) => {
    const body = underscore.pick(req.body, 'destination', 'password', 'otp', 'type');
    console.log(body);
    if (!body || body.length < 4) {
        return next(new CustomError(`Please send destination (email address or phone) , type , password and otp in request body`, 404));
    }
    if (body.type != 'email' && body.type != 'phone') {
        return next(new CustomError('Type can only be email or phone', 404));
    }
    let code = await db.verification_code.get({ where: { code: body.otp, destination: body.destination } });
    console.log('CODE')
    console.log(code);
    if (!code.data || body.error) {
        return next(new CustomError('Incorrect OTP. Please make sure the OTP is correct', 404));
    }
    if (code.data.expired()) {
        return next(new CustomError('OTP code has expired', 404));
    }
    if (code.data.used) {
        return next(new CustomError('OTP code already used', 404));
    }
    let update = await db.patient.modify({ password: body.password, token: '', token_hash: '' }, { where: { [body.type]: body.destination } });
    if (update.error) {
        return next(update.error)
    }
    await db.verification_code.modify({ used: true }, { where: { code: body.otp, destination: body.destination } })
    res.json({
        message: 'Password updated successfully'
    });
});

app.get('/verify/otp', async (req, res, next) => {
    let query = underscore.pick(req.query, 'destination', 'code');
    if (!query || Object.keys(query).length < 2) {
        return next(new CustomError('Please send destination (email address or phone number) and code in request query', 404));
    }
    let code = await db.verification_code.get({ where: { code: query.code, destination: query.destination, used: false } });
    if (code.data == null || code.error != null) {
        return next(new CustomError('Invalid code', 404));
    }
    await db.verification_code.modify({ used: true }, { where: { code: query.code, destination: query.destination } })
    res.json({
        message: 'Code verification successful'
    })
});





module.exports = app;