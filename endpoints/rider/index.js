const { Router } = require('express');
const app = Router();
const db = require('../../controllers/db.js');
const middleware = require('../../controllers/middleware.js')(db);
const underscore = require('underscore');

app.post('/login', (req, res, next) => {
    console.log(req.body);
    const body = underscore.pick(req.body, "username", "phone");
    if (!body || Object.keys(body).length < 2) {
        console.log('fuck');
        throw new CustomError('Request body is missing. Please send user and phone in request body', 400)
    }
    let token, riderInstance;
    db.rider
        .authenticate(body)
        .then(rider => {
            riderInstance = rider;
            token = rider.generateToken('authentication');
            return db.rider.update(
                {
                    token: token,
                },
                {
                    where: {
                        id: rider.id,
                    },
                }
            );
        })
        .then(u => {
            res.header('Authentication', token);
            var returnableRider = riderInstance.toPublicJSON();
            returnableRider.token = token;
            res.json({
                message: 'Login successful',
                rider: returnableRider
            })
        })
        .catch(err => {
            next(err);
        });
});

app.get('/list', middleware.authenticateAdmin, async (req, res, next) => {

    var limit = parseInt(req.query.limit) || 10;
    var page = parseInt(req.query.page) || 0;
    if (page >= 1) {
        page = page - 1;
    }


    var where = {};



    try {
        let rawWhere = JSON.parse(req.query.where || {}) || {};
        if (Object.keys(rawWhere).length > 0) {
            Object.keys(rawWhere).forEach(e => {
                if (['name', 'username', 'phone'].includes(e)) {
                    where[e] = {
                        [db.Op.like]: `%${rawWhere[e]}%`
                    }
                } else {
                    where[e] = rawWhere[e];
                }
            })
        }
    } catch (error) { }


    let { data, error } = await db.rider.getAllAndCount({
        where: where || {},
        limit: limit,
        offset: limit * page,
        order: [["createdAt", "DESC"]],
        distinct: true,
    });
    if (error) {
        next(error);
        return;
    }
    res.json(data);
});



module.exports = app;