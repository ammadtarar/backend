const { Router } = require('express');
const app = Router();
const underscore = require('underscore');
const db = require('../../controllers/db.js');
const middleware = require('../../controllers/middleware.js')(db);

const fs = require('fs');
var path = require('path');

app.post('/create', async (req, res, next) => {
    let { data, error } = await db.banner.add(req.body);
    if (error) {
        next(error);
        return
    }
    res.json({
        message: 'Banner addedd successfully'
    });
});

app.get('/list', async (req, res, next) => {
    let { data, error } = await db.banner.getAll();
    if (error) {
        next(error);
        return
    }
    res.json(data);
});


app.delete('/:id', middleware.authenticateAdmin, async (req, res, next) => {
    checkAdminLevelOneAccess(req, 'Only admin with access level 1 can delete banners');
    var id = parseInt(req.params.id);
    if (id === undefined || id === null || id <= 0) {
        throw new CustomError('Please send banner id in url. For example /banner/:id.', 400);
    }

    let { data, error } = await db.banner.get({ where: { id: id } });
    if (error) {
        next(error)
        return
    }
    if (data == null) {
        next(new CustomError(`Banner not found. Banner with ID = ${id} does not exist`, 404));
        return
    }
    await data.destroy();
    res.json({
        message: 'Banner deleted successfully'
    });

    // DELETE FILE
    let basicPath = data.img_url.replace(`${process.env.APP_URL}/file/`, '');
    let fullPath = path.resolve(`${__dirname}/../../uploads/${basicPath}`)
    fs.unlinkSync(fullPath)
});


module.exports = app;