const { Router } = require('express');
const app = Router();
const db = require('../../controllers/db.js');
const middleware = require('../../controllers/middleware.js')(db);
const fs = require('fs');
var path = require('path');

app.post('/create', middleware.authenticateAdmin , async (req, res, next) => {
    let { data, error } = await db.package.add(req.body);
    if (error) {
        next(error);
        return
    }
    res.json({
        message: 'Package addedd successfully'
    });
});

app.get('/list', async (req, res, next) => {
    let { data, error } = await db.package.getAll();
    if (error) {
        next(error);
        return
    }
    res.json(data);
});


app.patch('/:id', middleware.authenticateAdmin, async (req, res, next) => {
    checkAdminLevelOneAccess(req, 'Only admin with access level 1 can update packages');
    var id = parseInt(req.params.id);
    if (id === undefined || id === null || id <= 0) {
        throw new CustomError('Please send package id in url. For example /packages/:id.', 400);
    }

    // let { data, error } = await db.package.update(req.body, {
    //     where: {
    //         id: id
    //     }
    // });

    let { data, error } = await db.package.get({ where: { id: id } });

    if (error) {
        next(error)
        return
    }

    data = Object.assign( data , req.body)
    data.save()
    

    res.json({
        message: 'Package updated successfully'
    });

    


});


app.delete('/:id', middleware.authenticateAdmin, async (req, res, next) => {
    checkAdminLevelOneAccess(req, 'Only admin with access level 1 can delete packages');
    var id = parseInt(req.params.id);
    if (id === undefined || id === null || id <= 0) {
        throw new CustomError('Please send package id in url. For example /packages/:id.', 400);
    }

    let { data, error } = await db.package.get({ where: { id: id } });
    if (error) {
        next(error)
        return
    }
    if (data == null) {
        next(new CustomError(`Package not found. Banner with ID = ${id} does not exist`, 404));
        return
    }
    await data.destroy();
    res.json({
        message: 'Package deleted successfully'
    });

    // DELETE FILE
    let basicPath = data.cover_url.replace(`${process.env.APP_URL}/file/`, '');
    let fullPath = path.resolve(`${__dirname}/../../uploads/${basicPath}`)
    console.log('fullPath = ' , fullPath);
    console.log(fs.unlinkSync(fullPath));
});


module.exports = app;