const { Router } = require('express');
const app = Router();
const underscore = require('underscore');
const db = require('../../controllers/db.js');
const middleware = require('../../controllers/middleware.js')(db);


app.post('/create', async (req, res, next) => {
    const body = underscore.pick(req.body, 'email', 'name', 'city', 'score', 'message');
    if (!body || Object.keys(body).length < 4) {
        throw new CustomError('Please send send name , email , city and score in the request body');
    }
    let { data, error } = await db.feedback.add(body);
    if (error) {
        next(error);
        return
    }

    res.json({
        message: 'Feedback submitted successfully. Thank you for taking the time tell us how we are doing.'
    });
});

app.get('/list', middleware.authenticateAdmin, async (req, res, next) => {
    var limit = parseInt(req.query.limit) || 10;
    var page = parseInt(req.query.page) || 0;
    if (page >= 1) {
        page = page - 1;
    }

    let { data, error } = await db.feedback
        .getAllAndCount({
            where: req.body.where || {},
            limit: limit,
            offset: limit * page,
            order: [["createdAt", "DESC"]],
            distinct: true,
        });

    if (error) {
        next(error);
        return;
    }
    res.json(data);

});


app.delete('/:id', middleware.authenticateAdmin, async (req, res, next) => {
    checkAdminLevelOneAccess(req, 'Only admin with access level 1 can delete feedback');
    var id = parseInt(req.params.id);
    if (id === undefined || id === null || id <= 0) {
        throw new CustomError('Please send feedback id in url. For example /feedback/:id.', 400);
    }

    let { data, error } = await db.feedback.get({ where: { id: id } });
    if (error) {
        next(error)
        return
    }
    if (data == null) {
        next(new CustomError(`Feedback not found. feedback with ID = ${id} does not exist`, 404));
        return
    }
    await data.destroy();
    res.json({
        message: 'Feedback deleted successfully'
    });

});


module.exports = app;