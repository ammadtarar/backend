const { Router } = require('express');
const app = Router();
const underscore = require('underscore');
const db = require('../../controllers/db.js');
const middleware = require('../../controllers/middleware.js')(db);

app.post('/add', async (req, res, next) => {
    let body = underscore.pick(req.body, 'email', 'name');
    if (!body || Object.keys(body).length < 2) {
        throw new CustomError('Please send email and name in the request body');
    }

    let query = await db.subscriber.add(body);
    if (query.error) {
        next(error)
        return
    }
    res.json({ message: 'Thank you for subscribing to our newsletter' });

});

app.get('/list', middleware.authenticateAdmin, async (req, res, next) => {
    var limit = parseInt(req.query.limit) || 10;
    var page = parseInt(req.query.page) || 0;
    if (page >= 1) {
        page = page - 1;
    }

    let query = await db.subscriber.getAllAndCount({
        where: req.body.where || {},
        limit: limit,
        offset: limit * page,
        order: [["createdAt", "DESC"]],
        distinct: true,
    });

    if (query.error) {
        next(error)
        return
    }
    res.json(query.data);
});

app.delete('/:id', middleware.authenticateAdmin, async (req, res, next) => {
    checkAdminLevelOneAccess(req, 'Only admin with access level 1 can delete subscribers');
    var id = parseInt(req.params.id);
    if (id === undefined || id === null || id <= 0) {
        throw new CustomError('Please send subscriber id in url. For example /subscriber/:id.', 400);
    }

    let { data, error } = await db.subscriber.get({ where: { id: id } });
    if (error) {
        next(error)
        return
    }
    if (data == null) {
        next(new CustomError(`Subscriber not found. Subsriber with ID = ${id} does not exist`, 404));
        return
    }
    await data.destroy();
    res.json({
        message: 'Subscriber deleted successfully'
    });

});



module.exports = app;