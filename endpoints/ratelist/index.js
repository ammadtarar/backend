const { Router } = require('express');
const app = Router();
const db = require('../../controllers/db.js');

app.get('/tests', async (req, res, next) => {

    let { data, error } = await db.rateslist.getAll();
    if (error) {
        next(error);
        return
    }
    res.json(data);
});

module.exports = app;