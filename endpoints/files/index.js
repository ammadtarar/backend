const { Router } = require('express');
const app = Router();
const db = require('../../controllers/db.js');
const middleware = require('../../controllers/middleware.js')(db);
const fs = require('fs');
const fileUpload = require('express-fileupload');
var path = require('path');
app.use(fileUpload());

app.post('/upload', middleware.authenticateAdmin, function (req, res, next) {


    if (!req.files || Object.keys(req.files).length === 0) {
        throw new CustomError('Plase send a file in requst', 400);
    }
    if (!req.body.folder) {
        return res.status(400).send('Please send folder name in request body');
    }
    let folder = req.body.folder;

    let directory = path.resolve(uploadPath = __dirname + `/../../uploads/${folder}`);
    if (!fs.existsSync(directory)) {
        fs.mkdirSync(directory);
    }

    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    let tempFile = req.files.file;
    let fileName = tempFile.name.replace(/\s+/g, "_");
    let filePath = `${directory}/${fileName}`;


    // Use the mv() method to place the file somewhere on your server
    tempFile.mv(filePath, function (err) {
        if (err)
            return res.status(500).send(err);

        res.json({
            message: 'File uploaded',
            url: `${process.env.APP_URL}/file/${folder}/${fileName}`
        })
    });
});

app.get('/:folder/:name', function (req, res, next) {
    let folder = req.params.folder;
    if (folder === undefined || folder === null || folder == '') {
        throw new CustomError('Plaese send the file folder name in url . For example /file/:folder/:name', 400);
    }
    let name = req.params.name;
    if (name === undefined || name === null || name == '') {
        throw new CustomError('Plaese send the file name in url . For example /file/:folder/:name', 400);
    }
    let targetDirectory = path.resolve(`${__dirname + '/../../uploads'}/${folder}`)
    if (!fs.existsSync(targetDirectory)) {
        throw new CustomError(`Directory does not exist`, 404);
    }
    let fullPath = path.resolve(`${targetDirectory}/${name}`);
    if (!fs.existsSync(fullPath)) {
        throw new CustomError(`File does not exist`, 404);
    }
    res.sendFile(path.resolve(fullPath));
})

// http://localhost:3003/file/social/5296516_tweet_twitter_twitter_logo_icon.png
module.exports = app;