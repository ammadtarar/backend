const { Router } = require('express');
const app = Router();
const db = require('../../controllers/db.js');
const middleware = require('../../controllers/middleware.js')(db);


app.post('/create', middleware.authenticateAdmin , async (req, res, next) => {
    let { data, error } = await db.faq.add(req.body);
    if (error) {
        next(error);
        return
    }
    res.json({
        message: 'FAQ addedd successfully'
    });
});

app.get('/list', async (req, res, next) => {
    let { data, error } = await db.faq.getAll();
    if (error) {
        next(error);
        return
    }
    res.json(data);
});


app.patch('/:id', middleware.authenticateAdmin, async (req, res, next) => {
    checkAdminLevelOneAccess(req, 'Only admin with access level 1 can update FAQ');
    var id = parseInt(req.params.id);
    if (id === undefined || id === null || id <= 0) {
        throw new CustomError('Please send FAQ id in url. For example /packages/:id.', 400);
    }

    let { data, error } = await db.faq.get({ where: { id: id } });

    if (error) {
        next(error)
        return
    }

    data = Object.assign( data , req.body)
    data.save()
    
    res.json({
        message: 'FAQ updated successfully'
    });


});


app.delete('/:id', middleware.authenticateAdmin, async (req, res, next) => {
    checkAdminLevelOneAccess(req, 'Only admin with access level 1 can delete FAQs');
    var id = parseInt(req.params.id);
    if (id === undefined || id === null || id <= 0) {
        throw new CustomError('Please send FAQ id in url. For example /faq/:id.', 400);
    }

    let { data, error } = await db.faq.get({ where: { id: id } });
    if (error) {
        next(error)
        return
    }
    if (data == null) {
        next(new CustomError(`FAQ not found. FAQ with ID = ${id} does not exist`, 404));
        return
    }
    await data.destroy();
    res.json({
        message: 'FAQ deleted successfully'
    });

});


module.exports = app;