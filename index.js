require("custom-env").env(process.env.NODE_ENV);
require("./utils/custom-error");
require("./utils/helperFunctions");

const PORT = process.env.PORT || 3001;

console.log(`Your port is ${PORT}`);
const force = process.env.FORCE || false;

const app = require("./server");
const db = require("./controllers/db.js");

db.sequelize
    .sync({
        force: process.env.NODE_ENV === "production" ? false : force,
    })
    .then(() => {
        app.listen(PORT, () => {
            console.log();
            console.log();
            console.log("=======================================");
            console.log(`=== APP IS LISTENING ON PORT : ${PORT} ===`);
            console.log("=======================================");
            console.log();
            console.log();
            if (force) {
                createRootAdmin()
            }
        });
    });

let createRootAdmin = () => {
    db.admin.create({
        name: 'Root',
        email: 'root@nxgenlabs.com.pk',
        password: 'root@nxgenlabs',
        access_level: 1,
        gender: 'other'
    })
        .then(admin => {
            console.log();
            console.log();
            console.log('====== ROOT ADMIN CREATED ======');
            console.log(admin.toPublicJSON());
            console.log('================================');
            console.log();
            console.log();
        })

}
