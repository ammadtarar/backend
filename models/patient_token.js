const BaseModel = require('./BaseModel');
var cryptojs = require('crypto-js');
var jwt = require('jsonwebtoken');

module.exports = function (sequelize, DataTypes) {
    class Token extends BaseModel { }

    Token.init({
        token: {
            type: DataTypes.VIRTUAL,
            allowNull: true,
            set: function (value) {
                var hash = cryptojs.MD5(value).toString();
                this.setDataValue('token', value);
                this.setDataValue('tokenHash', hash);
            }
        },
        tokenHash: {
            type: DataTypes.STRING,
            allowNull: true
        },
    }, {
        sequelize,
        modelName: 'patient_token',
        underscored: true
    });

    Token.prototype.generateToken = function (type) {
        try {
            var stringData = JSON.stringify({
                id: this.get('patient_id'),
                type: type
            });
            var encryptedData = cryptojs.AES.encrypt(stringData, process.env.CRYPTO_SECRET).toString();
            var token = jwt.sign({
                token: encryptedData
            }, process.env.TOKEN_SECRET);
            return token;
        } catch (e) {
            throw e;
        }
    };




    return Token;
};