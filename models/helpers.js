get = (model, params) => {
    return new Promise((resolve, reject) => {
        model.findOne(params)
            .then(res => {
                resolve({
                    data: res,
                    error: null
                });
            })
            .catch(err => {
                resolve({
                    data: null,
                    error: err
                })
            })
    });
}

getAll = (model, params) => {
    return new Promise((resolve, reject) => {
        model.findAll(params)
            .then(res => {
                resolve({
                    data: res,
                    error: null
                });
            })
            .catch(err => {
                resolve({
                    data: null,
                    error: err
                })
            })
    });
}


getAllAndCount = (model, params) => {
    return new Promise((resolve, reject) => {
        model.findAndCountAll(params)
            .then(res => {
                resolve({
                    data: res,
                    error: null
                });
            })
            .catch(err => {
                resolve({
                    data: null,
                    error: err
                })
            })
    });
}


add = (model, params) => {


    return new Promise((resolve, reject) => {

        try {
            model.create(params)
            .then(res => {
                resolve({
                    data: res,
                    error: null
                });
            })
            .catch(err => {
                resolve({
                    data: null,
                    error: err
                })
            })
        } catch (error) {
            resolve({
                data: null,
                error: error
            })
        }
      
    });
}

bulkAdd = (model, params) => {
    return new Promise((resolve, reject) => {
        model.bulkCreate(params)
            .then(res => {
                resolve({
                    data: res,
                    error: null
                });
            })
            .catch(err => {
                resolve({
                    data: null,
                    error: err
                })
            })
    });
}


modify = (model, params) => {
    return new Promise((resolve, reject) => {
        model.update(params[0], params[1])
            .then(res => {
                resolve({
                    data: res,
                    error: null
                });
            })
            .catch(err => {
                resolve({
                    data: null,
                    error: err
                })
            })
    });
}

remove = (model, params) => {
    return new Promise((resolve, reject) => {
        model.destroy(params)
            .then(res => {
                resolve({
                    data: res,
                    error: null
                });
            })
            .catch(err => {
                resolve({
                    data: null,
                    error: err
                })
            })
    });
}






module.exports = {
    get,
    getAll,
    getAllAndCount,
    add,
    bulkAdd,
    modify,
    remove
}