
const BaseModel = require('./BaseModel');

module.exports = function (sequelize, DataTypes) {
    class RateList extends BaseModel { }

    RateList.init({
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        price: {
            type: DataTypes.STRING,
            allowNull: false
        },
        time: {
            type: DataTypes.STRING,
            allowNull: false
        },
        sample: {
            type: DataTypes.STRING,
            allowNull: false
        },
        is_available: {
            type: DataTypes.STRING,
            allowNull: true,
            defaultValue: 'A'
        }
    }, {
        underscored: true,
        sequelize,
        modelName: 'ratelist',
    });

    return RateList

};