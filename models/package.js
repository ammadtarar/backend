
const BaseModel = require('./BaseModel');

module.exports = function (sequelize, DataTypes) {
    class RateList extends BaseModel { }

    RateList.init({
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        price: {
            type: DataTypes.STRING,
            allowNull: false
        },
        cover_url: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        category: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        tests: {
            type: DataTypes.STRING,
            allowNull: false,
            set(value) {
                if(typeof value == 'string'){
                    throw new CustomError('Please send strings array for test' , 401);
                }
                console.log(typeof value);
                this.setDataValue('tests', JSON.stringify(value));
            },
            get(){
                let original = this.getDataValue('tests');
                try {
                    let parsed = JSON.parse(original);
                    return parsed
                } catch (error) {
                    return original
                }
            }
        }
    }, {
        underscored: true,
        sequelize,
        modelName: 'package',
    });

    return RateList

};