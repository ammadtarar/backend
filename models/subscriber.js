
const BaseModel = require('./BaseModel');

module.exports = function (sequelize, DataTypes) {
    class Subscriber extends BaseModel { }

    Subscriber.init({
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                isEmail: true
            }
        },
    }, {
        sequelize,
        modelName: 'subscriber',
        underscored: true
    });

    return Subscriber;
};