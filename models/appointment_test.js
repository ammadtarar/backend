
const BaseModel = require('./BaseModel');
const { APPOINTMENT_STATUS } = require('../controllers/constants.js');

module.exports = function (sequelize, DataTypes) {
    class AppointmentTest extends BaseModel { }

    AppointmentTest.init({
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        status: {
            type: DataTypes.ENUM(APPOINTMENT_STATUS),
            values: APPOINTMENT_STATUS,
            defaultValue: APPOINTMENT_STATUS[0],
            set(value) {
                if (!APPOINTMENT_STATUS.includes(value)) {
                    throw new EnumValidationError('incorrect appointment test status', 'status', APPOINTMENT_STATUS, value);
                }
                this.setDataValue('status', value);
            }
        },
        sample_status: {
            type: DataTypes.STRING,
            allowNull: true
        },
        test_detail_code: {
            type: DataTypes.STRING,
            allowNull: true
        },
        template_id: {
            type: DataTypes.STRING,
            allowNull: true
        },
        format: {
            type: DataTypes.STRING,
            allowNull: true
        },
        test_code: {
            type: DataTypes.STRING,
            allowNull: true
        }
    }, {
        sequelize,
        modelName: 'appointment_test',
        underscored: true
    });

    return AppointmentTest;

};