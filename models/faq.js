
const BaseModel = require('./BaseModel');

module.exports = function (sequelize, DataTypes) {
    class RateList extends BaseModel { }

    RateList.init({
        question: {
            type: DataTypes.STRING,
            allowNull: false
        },
        answer: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        underscored: true,
        sequelize,
        modelName: 'faq',
    });

    return RateList

};