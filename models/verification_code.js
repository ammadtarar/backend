const moment = require('moment');
const BaseModel = require('./BaseModel');

module.exports = function (sequelize, DataTypes) {
    class Code extends BaseModel { }

    Code.init({
        code: {
            type: DataTypes.INTEGER,
            allowNull: true,
            set: function () {
                console.log('SUP');
                this.setDataValue('code', Math.floor(100000 + Math.random() * 900000))
            }
        },
        destination: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: false
        },
        used: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
            defaultValue: false
        },
    }, {
        sequelize,
        modelName: 'verification_code',
        underscored: true
    });


    Code.beforeCreate(async (data, options) => {
        console.log('options');
        console.log(options);
        data.code = Math.floor(100000 + Math.random() * 900000)
    });



    Code.prototype.expired = function () {
        let diff = moment().diff(this.get('createdAt'), 'minutes');
        return diff <= 15 ? false : true
    }

    Code.prototype.expire = function () {
        console.log('EXPIRING');
        let a = this.setDataValue('used', true);
        console.log(a);
    }

    return Code;

};