
const BaseModel = require('./BaseModel');

module.exports = function (sequelize, DataTypes) {
    class Feedback extends BaseModel { }

    Feedback.init({
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                isEmail: true
            }
        },
        city: {
            type: DataTypes.STRING,
            allowNull: false
        },
        score: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        message: {
            type: DataTypes.STRING,
            allowNull: true
        }
    }, {
        sequelize,
        modelName: 'feedback',
        underscored: true
    });

    return Feedback;

};