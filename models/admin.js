const BaseModel = require('./BaseModel');
var bcrypt = require('bcrypt');
var _ = require('underscore');
var cryptojs = require('crypto-js');
var jwt = require('jsonwebtoken');
const { GENDERS } = require('../controllers/constants.js');


module.exports = function (sequelize, DataTypes) {

    const Admin = require('./BaseModel');

    Admin.init({
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            validate: {
                isEmail: true
            }
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        gender: {
            type: DataTypes.ENUM(GENDERS),
            values: GENDERS,
            defaultValue: GENDERS[0],
            set(value) {
                if (!GENDERS.includes(value)) {
                    throw new EnumValidationError('incorrect gender', 'gender', GENDERS, value);
                }
                this.setDataValue('gender', value);
            }
        },
        salt: {
            type: DataTypes.STRING
        },
        password_hash: {
            type: DataTypes.STRING
        },
        access_level: {
            type: DataTypes.INTEGER,
            allowNull: false,
            default: 1
        },
        password: {
            type: DataTypes.VIRTUAL,
            allowNull: false,
            validate: {
                len: [7, 100]
            },
            set: function (value) {
                var salt = bcrypt.genSaltSync(10);
                var hashedPassword = bcrypt.hashSync(value, salt);

                this.setDataValue('password', value);
                this.setDataValue('salt', salt);
                this.setDataValue('password_hash', hashedPassword);
            }
        },
        token: {
            type: DataTypes.VIRTUAL,
            allowNull: true,
            set: function (value) {
                var hash = cryptojs.MD5(value).toString();
                this.setDataValue('token', value);
                this.setDataValue('tokenHash', hash);
            }
        },
        tokenHash: {
            type: DataTypes.STRING,
            allowNull: true
        }
    }, {
        sequelize,
        modelName: 'admin',
        hooks: {
            beforeValidate: function (admin, options) {
                // admin.email
                if (typeof admin.email === 'string') {
                    admin.email = admin.email.toLowerCase();
                }
            }
        },
        underscored: true
    });



    Admin.findByToken = function (token) {
        return new Promise(function (resolve, reject) {
            try {
                const decodedJWT = jwt.verify(token, process.env.TOKEN_SECRET);
                const bytes = cryptojs.AES.decrypt(decodedJWT.token, process.env.CRYPTO_SECRET);
                const tokenData = JSON.parse(bytes.toString(cryptojs.enc.Utf8));

                Admin.findById(tokenData.id).then(function (admin) {
                    if (admin) {
                        resolve(admin);
                    } else {
                        reject({
                            status: 404,
                            message: "admin not found. Please make sure the token is valid and admin exists"
                        });
                    }
                }, function (e) {
                    reject(e);
                });
            } catch (e) {
                reject(e);
            }
        });
    };


    Admin.prototype.generateToken = function (type) {
        try {
            var stringData = JSON.stringify({
                id: this.get('id'),
                type: type
            });
            var encryptedData = cryptojs.AES.encrypt(stringData, process.env.CRYPTO_SECRET).toString();
            var token = jwt.sign({
                token: encryptedData
            }, process.env.TOKEN_SECRET);
            return token;
        } catch (e) {
            throw e;
        }
    };



    Admin.authenticate = function (body) {
        return new Promise(function (resolve, reject) {
            if (typeof body.email !== 'string' || typeof body.password !== 'string') {
                ;
                reject({
                    status: 409,
                    message: "Please send both email and password in request body"
                });
            }
            Admin.findOne({
                where: {
                    email: body.email
                }
            }).then(function (admin) {
                if (!admin || !bcrypt.compareSync(body.password, admin.get(
                    'password_hash'))) {
                    reject({
                        status: 404,
                        message: "Email or password is incorrect . Please make sure you enter a valid registered email with correct password"
                    });
                    return
                }
                resolve(admin);
            }, function (e) {
                reject();
            });
        });
    };

    Admin.prototype.toPublicJSON = function () {
        var json = this.toJSON();
        return _.pick(json, 'id', 'email', 'created_at', "name", "gender", "access_level");
    };

    return Admin;
};