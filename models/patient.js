var bcrypt = require('bcrypt');
var _ = require('underscore');

const { GENDERS } = require('../controllers/constants.js');
const BaseModel = require('./BaseModel');


module.exports = function (sequelize, DataTypes) {

    class Patient extends BaseModel { }

    Patient.init({
        email: {
            type: DataTypes.STRING,
            allowNull: true,
            unique: true,
            validate: {
                isEmail: true
            }
        },
        phone: {
            unique: true,
            type: DataTypes.STRING,
            allowNull: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        gender: {
            type: DataTypes.ENUM(GENDERS),
            values: GENDERS,
            defaultValue: GENDERS[0],
            set(value) {
                if (!GENDERS.includes(value)) {
                    throw new EnumValidationError('incorrect gender', 'gender', GENDERS, value);
                }
                this.setDataValue('gender', value);
            }
        },
        address: {
            type: DataTypes.STRING,
            allowNull: false
        },
        city: {
            type: DataTypes.STRING,
            allowNull: false
        },
        salt: {
            type: DataTypes.STRING
        },
        password_hash: {
            type: DataTypes.STRING
        },
        password: {
            type: DataTypes.VIRTUAL,
            allowNull: false,
            validate: {
                len: [7, 100]
            },
            set: function (value) {
                var salt = bcrypt.genSaltSync(10);
                var hashedPassword = bcrypt.hashSync(value, salt);

                this.setDataValue('password', value);
                this.setDataValue('salt', salt);
                this.setDataValue('password_hash', hashedPassword);
            }
        },
        lims_computerno: {
            type: DataTypes.INTEGER,
            allowNull: true
        }
    }, {
        sequelize,
        modelName: 'patient',
        hooks: {
            beforeValidate: function (patient, options) {
                // patient.email
                if (typeof patient.email === 'string') {
                    patient.email = patient.email.toLowerCase();
                }
            }
        },
        underscored: true
    });


    Patient.authenticate = function (body) {
        return new Promise(function (resolve, reject) {
            if (!body.type || typeof body.username !== 'string' || typeof body.password !== 'string') {
                ;
                reject({
                    status: 409,
                    message: "Please send both username and password in request body"
                });
            }

            if (body.type != 'email' && body.type != 'phone') {
                reject({
                    status: 409,
                    message: "Type can only be phone or email"
                });
            }


            Patient.findOne({
                where: {
                    [body.type]: body.username
                }
            }).then(function (patient) {
                if (!patient || !bcrypt.compareSync(body.password, patient.get(
                    'password_hash'))) {
                    reject({
                        status: 404,
                        message: "Mobile number or password is incorrect . Please make sure you enter a valid registered mobile number with correct password"
                    });
                    return
                }
                resolve(patient);
            }, function (e) {
                reject(e);
            });
        });
    };

    Patient.prototype.toPublicJSON = function () {
        var json = this.toJSON();
        return _.pick(json, 'id', 'email', 'created_at', "name", "phone", "gender", "phone", "city", "address");
    };

    return Patient;
};