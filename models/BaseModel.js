
const helper = require('./helpers');
const { Model } = require('sequelize');


class BaseModel extends Model {
    static get(params) {
        return helper.get(this, params)
    }

    static getAll(params) {
        return helper.getAll(this, params)
    }

    static getAllAndCount(params) {
        return helper.getAllAndCount(this, params)
    }

    static add(params) {
        return helper.add(this, params)
    }

    static bulkAdd(params) {
        return helper.bulkAdd(this, params)
    }

    static modify(...args) {
        return helper.modify(this, args)
    }

    static remove(params) {
        return helper.remove(this, params)
    }


}

module.exports = BaseModel;