const BaseModel = require('./BaseModel');
const { GENDERS, APPOINTMENT_MODES } = require('../controllers/constants.js');

module.exports = function (sequelize, DataTypes) {
    class Appointment extends BaseModel { }

    Appointment.init({
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        email: {
            type: DataTypes.STRING,
            allowNull: true
        },
        phone: {
            type: DataTypes.STRING,
            allowNull: true
        },
        dob: {
            type: DataTypes.DATE,
            allowNull: false
        },
        gender: {
            type: DataTypes.ENUM(GENDERS),
            values: GENDERS,
            defaultValue: GENDERS[0],
            set(value) {
                if (!GENDERS.includes(value)) {
                    throw new EnumValidationError('incorrect gender', 'gender', GENDERS, value);
                }
                this.setDataValue('gender', value);
            }
        },
        mode: {
            type: DataTypes.ENUM(APPOINTMENT_MODES),
            values: APPOINTMENT_MODES,
            defaultValue: APPOINTMENT_MODES[0],
            set(value) {
                if (!APPOINTMENT_MODES.includes(value)) {
                    throw new EnumValidationError('incorrect mode', 'mode', APPOINTMENT_MODES, value);
                }
                this.setDataValue('mode', value);
            }
        },
        address: {
            type: DataTypes.STRING,
            allowNull: true
        },
        city: {
            type: DataTypes.STRING,
            allowNull: true
        },
        time: {
            type: DataTypes.DATE,
            allowNull: false
        },
        lims_patient_number: {
            type: DataTypes.STRING,
            allowNull: true
        },
        lims_computerno: {
            type: DataTypes.INTEGER,
            allowNull: true
        }
    }, {
        sequelize,
        modelName: 'appointment',
        underscored: true
    });

    return Appointment;
};