
const BaseModel = require('./BaseModel');

module.exports = function (sequelize, DataTypes) {
    class Banner extends BaseModel { }

    Banner.init({
        img_url: {
            type: DataTypes.STRING,
            allowNull: false
        },
        redirect_link: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        name: {
            type: DataTypes.STRING,
            allowNull: true,
        }
    }, {
        sequelize,
        modelName: 'banner',
        underscored: true
    });

    return Banner;
};