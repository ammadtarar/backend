
const BaseModel = require('./BaseModel');
var _ = require('underscore');
var cryptojs = require('crypto-js');
var jwt = require('jsonwebtoken');


module.exports = function (sequelize, DataTypes) {
    class Rider extends BaseModel { }
    Rider.init({
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        username: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                isEmail: true
            }
        },
        phone: {
            type: DataTypes.STRING,
            allowNull: false
        },
        token: {
            type: DataTypes.VIRTUAL,
            allowNull: true,
            set: function (value) {
                var hash = cryptojs.MD5(value).toString();
                this.setDataValue('token', value);
                this.setDataValue('tokenHash', hash);
            }
        },
        tokenHash: {
            type: DataTypes.STRING,
            allowNull: true
        }
    }, {
        sequelize,
        modelName: 'rider',
        underscored: true
    })

    Rider.prototype.generateToken = function (type) {
        try {
            var stringData = JSON.stringify({
                id: this.get('id'),
                type: type,
                owner: 'rider'
            });
            var encryptedData = cryptojs.AES.encrypt(stringData, process.env.CRYPTO_SECRET).toString();
            var token = jwt.sign({
                token: encryptedData
            }, process.env.TOKEN_SECRET);
            return token;
        } catch (e) {
            throw e;
        }
    };


    Rider.authenticate = function (body) {
        console.log('authenticate');
        console.log(body);
        return new Promise(function (resolve, reject) {
            if (typeof body.username !== 'string' || typeof body.phone !== 'string') {
                ;
                reject({
                    status: 409,
                    message: "Please send both email and password in request body"
                });
            }
            console.log('finding');
            Rider.findOne({
                where: {
                    username: body.username,
                    phone: body.phone
                }
            }).then(function (rider) {
                console.log('inside');
                console.log(rider);
                if (!rider) {
                    reject({
                        status: 404,
                        message: "Username or phone number is incorrect"
                    });
                    return
                }
                resolve(rider);
            }, function (e) {
                console.log(e);
                reject(e);
            });
        });
    };




    Rider.findByToken = function (token) {
        return new Promise(function (resolve, reject) {
            try {
                const decodedJWT = jwt.verify(token, process.env.TOKEN_SECRET);
                const bytes = cryptojs.AES.decrypt(decodedJWT.token, process.env.CRYPTO_SECRET);
                const tokenData = JSON.parse(bytes.toString(cryptojs.enc.Utf8));

                Rider.findById(tokenData.id).then(function (rider) {
                    if (rider) {
                        resolve(rider);
                    } else {
                        reject({
                            status: 404,
                            message: "Rider not found. Please send a correct and valid authentictaion token"
                        });
                    }
                }, function (e) {
                    reject(e);
                });
            } catch (e) {
                reject(e);
            }
        });
    };

    Rider.prototype.toPublicJSON = function () {
        var json = this.toJSON();
        return _.pick(json, 'id', 'username', 'phone', "name",);
    };



    return Rider;
};

