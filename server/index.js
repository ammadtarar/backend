const express = require("express");

const server = express();


const db = require("../controllers/db.js");
const middleware = require("../controllers/middleware.js")(db);

let cors = require("cors");
server.use(cors());
server.use(express.json());
server.use(express.urlencoded());
server.use(middleware.logger);

server.use('', require('../endpoints/index.js'));

server.use(middleware.errorHandler);

module.exports = server;
